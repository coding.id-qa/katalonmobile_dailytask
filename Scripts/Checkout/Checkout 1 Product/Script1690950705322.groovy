import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\rizky\\Downloads\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.ImageView'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - Log In'), 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText'), 'bob@example.com', 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText (1)'), '10203040', 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.view.ViewGroup'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.ImageView (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - Add To Cart'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - 1'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - Proceed To Checkout'), 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - Rebecca Winter'), 'rizkyisya', 
    0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - Mandorley 112'), 'Jl.Pramuka', 
    0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - Truro'), 'Banjarmasin', 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - 89750'), '111222', 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - United Kingdom'), 'Indonesia', 
    0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - To Payment'), 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - Rebecca Winter (1)'), 'rizkyisya', 
    0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - 3258 1265 7568 789'), '3255265575687891', 
    0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - 0325'), '03/26', 0)

Mobile.setText(findTestObject('Object Repository/Record Checkout1/android.widget.EditText - 123'), '123', 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - Review Order'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.view.ViewGroup (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Record Checkout1/android.widget.TextView - Place Order'), 0)

Mobile.getText(findTestObject('Object Repository/Record Checkout1/android.widget.TextView -  Your order has been dispatched and will arrive as fast as the pony gallops'), 
    0)

Mobile.closeApplication()

